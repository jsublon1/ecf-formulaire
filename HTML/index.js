//---------Variables------------
let motifChoisi = false;

//---------Fonctions------------
function afficherDuree(radio) {
  const choixPeriode = document.querySelector("#duree");
  if (radio.value === "journee") {
    choixPeriode.innerHTML = `
        <div class="row">
            <label for="date-jour" class="col-7">Le :</label>
             <input type="date" name="date-jour" class="col-2" required/>
        </div>
        <div class="row">
          <label for="heure-debut" class="col-7">De :</label>
         <select name="heure-debut" id="heure-debut" class="col-2" required>
             <option value="08" selected>08:00</option>
            <option value="09">09:00</option>
            <option value="10">10:00</option>
            <option value="11">11:00</option>
            <option value="13">13:00</option>
            <option value="14">14:00</option>
            <option value="15">15:00</option>
            <option value="16">16:00</option>
        </select>
     </div>
    <div class="row">
      <label for="heure-fin" class="col-7">A :</label>
      <select name="heure-fin" id="heure-fin" class="col-2" required>
        <option value="09" selected>09:00</option>
        <option value="10">10:00</option>
        <option value="11">11:00</option>
        <option value="12">12:00</option>
        <option value="14">14:00</option>
        <option value="15">15:00</option>
        <option value="16">16:00</option>
        <option value="17">17:00</option>
        </select>
    </div>
  `;
  }
  if (radio.value === "plusieurs-jours") {
    choixPeriode.innerHTML = `
     <div class="row">
         <label for="debut-jour" class="col-7">Du :</label>
         <input
                type="date"
                name="debut-jour"
                id="debut-jour"
                class="col-2"
                required />
     </div>
    <div class="row">
        <label for="fin-jour" class="col-7">Au :</label>
        <input type="date" name="fin-jour" class="col-2" required/>
    </div>
    `;
  }
}

function recupererValeur(input) {
  let url = window.location.search.substring(1);
  let param = url.split("&");
  let result;
  param.forEach((elem) => {
    let tabPara = elem.split("=");
    if (tabPara[0] === input) result = tabPara[1];
  });
  return result;
}
function mettreEnGris(value) {
  document.querySelectorAll(".gris " + value).forEach((elem) => {
    elem.classList.add("gris");
  });
}

function validationEntree() {
  let formOK = false;
  let dateOK = false;
  let message = "";
  if (
    document.querySelector('input[name="periode"]:checked').value === "journee"
  ) {
    if (
      document.getElementsByName("heure-debut")[0].value <
      document.getElementsByName("heure-fin")[0].value
    ) {
      console.log(document.getElementsByName("heure-debut")[0].value);
      console.log(document.getElementsByName("heure-fin")[0].value);
      dateOK = true;
    } else {
      message += "L'heure de fin est avant l'heure du début\n";
    }
  }
  if (
    document.querySelector('input[name="periode"]:checked').value ===
    "plusieurs-jours"
  ) {
    if (
      document.getElementsByName("debut-jour")[0].value <
      document.getElementsByName("fin-jour")[0].value
    ) {
      dateOK = true;
    } else {
      message += "Le jour de fin n'est pas après le jour du début\n";
    }
  }
  if (!motifChoisi) message += "Aucun Motif n'a été choisi";
  if (dateOK && motifChoisi) {
    formOK = true;
  } else {
    alert(message);
  }
  return formOK;
}
//-------Chargement du fichier JSON----------------

const requeteURL = "/Ressources/motif.json";
const requete = new XMLHttpRequest();
const partieMotifs = document.querySelector("#motifs");

function check(input) {
  let checkboxes = document.getElementsByName("raison");
  motifChoisi = input.checked;
  for (let i = 0; i < checkboxes.length; i++) checkboxes[i].checked = false;
  input.checked = motifChoisi;
}
function creerDiv(jsonObj) {
  let motifs = jsonObj["members"];
  motifs.forEach((element) => {
    let newDiv = document.createElement("div");
    let newTitle = document.createElement("h3");
    newTitle.innerText = element.title;
    newDiv.appendChild(newTitle);
    partieMotifs.appendChild(newDiv);
    element.options.forEach((opt) => {
      newDiv.appendChild(creerCheckBox(opt));
    });
  });
}
function creerCheckBox(option) {
  let newDiv = document.createElement("div");
  newDiv.classList.add("boite-grise");
  newDiv.title = option.op_label;
  let newCheck = document.createElement("input");

  newCheck.type = "checkbox";
  newCheck.name = "raison";
  newCheck.id = option.op_id;
  newCheck.value = option.op_id;
  // newCheck.setAttribute("onclick", "check(this)");
  newCheck.addEventListener("click", (e) => {
    check(newCheck);
    e.stopPropagation();
  });
  if (document.title === "Formulaire d'absence - signature") {
    newCheck.disabled = "disabled";
    newDiv.classList.remove("boite-grise");
    newDiv.classList.add("boite-verte");
    if (newCheck.id === recupererValeur("raison")) newCheck.checked = true;
    else newCheck.style.visibility = "hidden";
  }

  let newLabel = document.createElement("label");
  newLabel.classList.add("boutonRadio");
  newLabel.innerText = option.op_label;

  newDiv.appendChild(newCheck);
  newDiv.appendChild(newLabel);
  newDiv.addEventListener("click", (e) => {
    newCheck.click();
  });
  // this.children[0].click());
  return newDiv;
}

// requete.open("GET", requeteURL, false);
// requete.addEventListener("load", function () {
//   let motifs = JSON.parse(requete.responseText);
//   creerDiv(motifs);
// });
// requete.send(null);

fetch(requeteURL)
  .then(function (res) {
    if (res.ok) {
      return res.json();
    }
  })
  .then(function (value) {
    creerDiv(value);
  })
  .catch(function (err) {
    console.error(err);
  });

if (document.title === "Formulaire d'absence - signature") {
  document.getElementsByName("nom")[0].value = recupererValeur("nom");
  document.getElementsByName("prenom")[0].value = recupererValeur("prenom");
  document
    .getElementById(recupererValeur("formation"))
    .setAttribute("selected", true);

  if (recupererValeur("periode") === "journee") {
    document.getElementsByName("date-jour")[0].value =
      recupererValeur("date-jour");
    document.getElementsByName("heure-debut")[0].value =
      recupererValeur("heure-debut");
    document.getElementsByName("heure-fin")[0].value =
      recupererValeur("heure-fin");
    document.getElementsByClassName("multi")[0].classList.add("gris");
    mettreEnGris("div");
    mettreEnGris("label");
    mettreEnGris("input");
  }
  if (recupererValeur("periode") === "plusieurs-jours") {
    document.getElementsByName("debut-jour")[0].value =
      recupererValeur("debut-jour");
    document.getElementsByName("fin-jour")[0].value =
      recupererValeur("fin-jour");
    document.getElementsByClassName("un-jour")[0].classList.add("gris");
    mettreEnGris("div");
    mettreEnGris("label");
    mettreEnGris("input");
    mettreEnGris("select");
  }
}
